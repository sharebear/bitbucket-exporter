#!/bin/bash

exec docker run --rm -p 9999:8080 -p 9443:8443 -v $PWD/wiremock:/home/wiremock rodolpheche/wiremock --https-port 8443
