#!/usr/bin/env bash

set -o errexit
set -o pipefail

if [ -z "${BITBUCKET_URL}" ]; then
  echo "Must set BITBUCKET_URL environment variable"
  exit 1
fi

if [ -z "${BITBUCKET_TOKEN}" ]; then
  echo "Must set BITBUCKET_TOKEN environment variable to a valid Personal access token"
  exit 1
fi

CERT_OPTIONS=""
if [ -n "${BITBUCKET_ROOT_CERT}" ]; then
  CERT_OPTIONS=" --add-root-certificate ${BITBUCKET_ROOT_CERT} "
fi

DISABLE_VERIFICATION=""
if [ "${BITBUCKET_DISABLE_HOSTNAME_VERIFICATION}" == "true" ]; then
  DISABLE_VERIFICATION=" --disable-hostname-verification "
fi

if [ "$1" == "test" ]; then
  exec /opt/bitbucket-exporter --test --token "${BITBUCKET_TOKEN}" ${CERT_OPTIONS} ${DISABLE_VERIFICATION} "${BITBUCKET_URL}"
fi

if [ -n "$1" ]; then
  echo "Invalid usage. The only permitted command line variable is \"test\""
  exit 1
fi

exec /opt/bitbucket-exporter --interval "${BITBUCKET_INTERVAL:-10}" --token "${BITBUCKET_TOKEN}" ${CERT_OPTIONS} ${DISABLE_VERIFICATION} "${BITBUCKET_URL}"
