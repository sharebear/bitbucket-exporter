FROM debian:stretch-slim

RUN apt-get update && apt-get upgrade -y && apt-get install libssl1.1 && rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /opt/entrypoint.sh

COPY target/release/bitbucket-exporter /opt/bitbucket-exporter

ENTRYPOINT ["/opt/entrypoint.sh"]
