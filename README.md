# bitbucket-exporter

A Prometheus exporter intended to scrape Atlassian Bitbucket Server for metrics that could be interesting to monitor 
for a team using Bitbucket Server.

## Status

I consider the functionality good enough for a v1 in it's current state, I just need to work out how to tweak my ci
config to cut a release.

## Why?

Personal experience shows that occasionally, especially when working across multiple repositories, pull requests get
lost or forgotten, especially if there is otherwise no activity on the pull requests triggering email notifications. In
my opinion pull requests in a truly agile/lean team should have a very short lifetime (hours not days) and think that 
pushing some simple stats on to a dashboard could help to visualise any bottlenecks.

Some googling reveals that there already exists a prometheus exporter plugin for Bitbucket server but the metrics
exposed appear to be focused on administrators of the server rather than users of the service, therefore there could be
a market for something more.

## Why Rust?

After many years as a Java developer, recently gaining an appreciation for a functional style of programming I'm very
curious about languages that a) have strong type systems and b) do not have null values. I could probably have
implemented this in a few lines of javascript but felt the task was small and concrete enough that it was a good match 
for my first attempt at something in Rust.

## Usage (docker/kubernetes)

Prometheus is commonly used in tandem with the service discovery features available via Kubernetes, this is exactly my
environment and therefore the project is designed to be used in this manner. If your BitBucket Server is served over
http or over https with a certificate trusted in the default trust store in Debian Stretch then all you need is
something similar to the following yaml

```yaml
apiVersion: apps/v1
kind: Deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: bitbucket-exporter
  template:
    metadata:
      labels:
        app: bitbucket-exporter
    spec:
      containers:
      - name: bitbucket-exporter
        image: registry.gitlab.com/sharebear/bitbucket-exporter:master-f956a68e059a9accf16397cad76b0cc99e6268ac
        env:
        - name: BITBUCKET_URL
          value: "<base url of your bitbucket server>"
        - name: BITBUCKET_TOKEN
          value: "<personal access token for the account to acccess the server as>"
---
apiVersion: v1
kind: Service
metadata:
  name: bitbucket-exporter
  annotations:
    prometheus.io/path: /metrics
    prometheus.io/port: "3000"
    prometheus.io/scrape: "true"
spec:
  selector:
    app: bitbucket-exporter
  ports:
  - protocol: TCP
    port: 3000
    targetPort: 3000
```

If you need to trust another certificate you can mount a pem formatted public key using a volume mount then provide a
path to the certificate with the `BITBUCKET_ROOT_CERT` environment variable.

## Usage (standalone)

Hopefully the online help is self explanatory.

```
bitbucket-exporter 

USAGE:
    bitbucket-exporter [FLAGS] [OPTIONS] <BITBUCKET_URL> --token <token>

FLAGS:
        --disable-hostname-verification    Disable hostname verification of https connections
    -h, --help                             Prints help information
    -t, --test                             Collects metrics exactly once and prints prometheus formatted metrics to
                                           standard out
    -V, --version                          Prints version information

OPTIONS:
    -i, --interval <interval>
            Interval with which the BitBucket API is scraped for metrics in minutes [default: 10]

        --add-root-certificate <root_certificate>
            Path to certificate that should be trusted for self signed http requests

        --token <token>                              Personal access token used to access the BitBucket server APIs

ARGS:
    <BITBUCKET_URL>    Base url of BitBucket server

```

## Testing

In order to not require constant connection to a live Bitbucket server while testing I am building out a suite of stubs
using WireMock. Wiremock can be easily started within a docker container by utilising the provided `startStubServer.sh`
shell script.

If you run the application with the --test flag you should get output to standard output that looks similar to the 
following.

```
# HELP bitbucket_open_pullrequest_count Number of open pull requests
# TYPE bitbucket_open_pullrequest_count gauge
bitbucket_open_pullrequest_count{project="PRJ",repository="my-repo"} 1
bitbucket_open_pullrequest_count{project="PRJ2",repository="boring-repo"} 1
bitbucket_open_pullrequest_count{project="PRJ3",repository="paged-repo"} 1
```

## TODO

* Add metric for how long scraping takes
* Consider adding metric for scraping errors
* Expose endpoint that can be used for k8s readinessProbe (should reflect if we are able to make contact with BB server)
* Delete old metrics automatically if repo/proj deleted
* Expand with other metrics that can be scraped / calculated
* Refactor `*List` types with a common `PagedList<T>` type
* Investigate if any of the current stringly typed fields can be better
* Investigate if type system can be used instead of bools some places (perhaps more interesting for a generic api)
