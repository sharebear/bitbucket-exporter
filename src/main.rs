extern crate reqwest;
#[macro_use]
extern crate serde_derive;
extern crate chrono;
#[macro_use]
extern crate prometheus;
extern crate hyper;
extern crate crossbeam_channel;
extern crate url;
extern crate structopt;

use reqwest::Client;
use reqwest::ClientBuilder;
use reqwest::Certificate;
use reqwest::header::Authorization;
use reqwest::header::Bearer;
use chrono::DateTime;
use chrono::Utc;
use chrono::serde::ts_seconds::deserialize as from_ts;
use prometheus::TextEncoder;
use prometheus::Encoder;
use hyper::{Body, Request, Response};
use hyper::service::service_fn_ok;
use hyper::Server;
use hyper::rt::Future;
use hyper::Method;
use hyper::StatusCode;
use prometheus::IntGaugeVec;
use crossbeam_channel as channel;
use std::time::Duration;
use std::thread;
use reqwest::Url;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct ProjectList {
    size: u16,
    limit: u16,
    is_last_page: bool,
    next_page_start: Option<u16>,
    values: Vec<ProjectListItem>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct ProjectListItem {
    key: String,
    id: u16,
    name: String,
    description: Option<String>,
    public: bool,
    #[serde(rename = "type")]
    project_type: String,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct RepositoryList {
    limit: u16,
    is_last_page: bool,
    next_page_start: Option<u16>,
    values: Vec<RepositoryListItem>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct RepositoryListItem {
    slug: String,
    id: u16,
    name: String,
    scm_id: String,
    state: String,
    status_message: String,
    forkable: bool,
    project: ProjectListItem,
    public: bool,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct PullRequestList {
    size: u16,
    limit: u16,
    is_last_page: bool,
    next_page_start: Option<u16>,
    values: Vec<PullRequestListItem>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct PullRequestListItem {
    id: u16,
    version: u16,
    title: String,
    description: Option<String>,
    state: String,
    open: bool,
    closed: bool,
    #[serde(deserialize_with = "from_ts")]
    created_date: DateTime<Utc>,
    #[serde(deserialize_with = "from_ts")]
    updated_date: DateTime<Utc>,
    // from_ref: Not necessary for current use-cases, can be mapped later
    // to_ref: Not necessary for current use-cases, can be mapped later
    locked: bool,
    // author: Not necessary for current use-cases, can be mapped later
    // reviewers: Not necessary for current use-cases, can be mapped later
    // participants: Not necessary for current use-cases, can be mapped later
}

#[derive(Debug)]
enum BitBucketError {
    Reqwest(reqwest::Error),
    Parse(url::ParseError),
}

impl From<reqwest::Error> for BitBucketError {
    fn from(err: reqwest::Error) -> Self {
        BitBucketError::Reqwest(err)
    }
}

impl From<url::ParseError> for BitBucketError {
    fn from(err: url::ParseError) -> Self {
        BitBucketError::Parse(err)
    }
}

struct BitBucketServer {
    url: Url,
    client: Client,
    access_token: String,
}

impl BitBucketServer {
    fn get_project_list(&self, start_project: u16) -> Result<ProjectList, BitBucketError> {
        let mut project_list_url = self.url.join("rest/api/1.0/projects")?;
        project_list_url.query_pairs_mut().append_pair("start", start_project.to_string().as_str());
        let project_list = self.client
            .get(project_list_url)
            .header(Authorization(Bearer { token: self.access_token.to_owned() }))
            .send()?
            .error_for_status()?
            .json()?;
        Ok(project_list)
    }

    fn get_repository_list(&self, key: &str, start_repository: u16) -> Result<RepositoryList, BitBucketError> {
        let mut repository_list_url = self.url.join(format!("rest/api/1.0/projects/{}/repos", key).as_str())?;
        repository_list_url.query_pairs_mut().append_pair("start", start_repository.to_string().as_str());
        let repository_list = self.client
            .get(repository_list_url)
            .header(Authorization(Bearer { token: self.access_token.to_owned() }))
            .send()?
            .error_for_status()?
            .json()?;
        Ok(repository_list)
    }

    fn get_pull_request_list(&self, key: &str, slug: &str, start_pull_request: u16) -> Result<PullRequestList, BitBucketError> {
        let mut pr_list_url = self.url.join(format!("rest/api/1.0/projects/{}/repos/{}/pull-requests", key, slug).as_str())?;
        pr_list_url.query_pairs_mut().append_pair("start", start_pull_request.to_string().as_str());
        let pr_list = self.client
            .get(pr_list_url)
            .header(Authorization(Bearer { token: self.access_token.to_owned() }))
            .send()?
            .error_for_status()?
            .json()?;
        Ok(pr_list)
    }
}

fn scrape_api(pull_request_counts: &IntGaugeVec, server: &BitBucketServer) -> Result<(), BitBucketError> {
    let mut start_project = Some(0);
    while start_project.is_some() {
        let project_list = server.get_project_list(start_project.expect("must be set here"))?;
        project_list.values.iter()
            .try_for_each(|project| scrape_project(pull_request_counts, server, &project))?;
        start_project = project_list.next_page_start;
    }
    Ok(())
}

fn scrape_project(pull_request_counts: &IntGaugeVec, server: &BitBucketServer, project: &ProjectListItem) -> Result<(), BitBucketError> {
    let mut start_repository = Some(0);
    while start_repository.is_some() {
        let repository_list = server.get_repository_list(&project.key, start_repository.expect("must be set here"))?;
        repository_list.values.iter()
            .try_for_each(|repository| scrape_repository(pull_request_counts, server, repository))?;
        start_repository = repository_list.next_page_start;
    }
    Ok(())
}

fn scrape_repository(pull_request_counts: &IntGaugeVec, server: &BitBucketServer, repository: &RepositoryListItem) -> Result<(), BitBucketError> {
    let mut pull_request_count = 0;
    let mut start_pull_request = Some(0);
    while start_pull_request.is_some() {
        let pull_request_list = server.get_pull_request_list(&repository.project.key, &repository.slug, start_pull_request.expect("must be set her"))?;
        pull_request_count += pull_request_list.size;
        start_pull_request = pull_request_list.next_page_start;
    }
    pull_request_counts
        .with(&labels! { "project" => repository.project.key.as_str(), "repository" => &repository.slug.as_str(),})
        .set(i64::from(pull_request_count));
    Ok(())
}

#[derive(Debug,StructOpt)]
#[structopt()]
struct Opt {
    #[structopt(short = "t", long = "test", conflicts_with = "interval")]
    /// Collects metrics exactly once and prints prometheus formatted metrics to standard out
    test: bool,

    #[structopt(short = "i", long = "interval", default_value = "10")]
    /// Interval with which the BitBucket API is scraped for metrics in minutes
    interval: u64,

    #[structopt(long = "add-root-certificate", empty_values = false)]
    /// Path to certificate that should be trusted for self signed http requests
    root_certificate: Option<PathBuf>,

    #[structopt(long = "disable-hostname-verification")]
    /// Disable hostname verification of https connections
    disable_hostname_verification: bool,

    #[structopt(long = "token")]
    /// Personal access token used to access the BitBucket server APIs
    token: String,

    #[structopt(name = "BITBUCKET_URL")]
    /// Base url of BitBucket server
    bitbucket_url: Url,
}

fn main() {
    let opt: Opt = Opt::from_args();

    // FIXME: This client and server are implementation details of the BitBucketServer and could
    // therefore be encapsulated inside some kind of constructor function
    let mut client_builder = ClientBuilder::new();

    if let Some(filename) = opt.root_certificate {
        let mut buf = Vec::new();
        File::open(filename).unwrap().read_to_end(&mut buf).unwrap();
        let cert = Certificate::from_pem(&buf).unwrap();
        client_builder.add_root_certificate(cert);
    }

    if opt.disable_hostname_verification {
        client_builder.danger_disable_hostname_verification();
    }

    let client = client_builder.build().unwrap();
    let url = opt.bitbucket_url;
    let access_token = opt.token;
    let server = BitBucketServer { url, client, access_token };

    // FIXME: Later iteration might want to publish total merged/open/declined in a single metric
    let pull_request_counts = register_int_gauge_vec!(
        "bitbucket_open_pullrequest_count",
        "Number of open pull requests",
        &["project", "repository"]
        ).unwrap();

    scrape_api(&pull_request_counts, &server).unwrap();

    if opt.test {
        // Quick n dirty dump of metrics state for prototyping now, ripped from an example
        let mut buffer = Vec::new();
        let encoder = TextEncoder::new();

        let metric_familys = prometheus::gather();
        encoder.encode(&metric_familys, &mut buffer).unwrap();
        println!("{}", String::from_utf8(buffer.clone()).unwrap());
        buffer.clear();
    } else {
        let interval_minutes = opt.interval;

        let interval_seconds = interval_minutes * 60;

        let receiver = channel::tick(Duration::from_secs(interval_seconds));
        thread::spawn(move || {
            loop {
                receiver.recv().unwrap();
                scrape_api(&pull_request_counts, &server).unwrap();
            }
        });

        let service = || {
            service_fn_ok(|req: Request<Body>| {
                match (req.method(), req.uri().path()) {
                    (&Method::GET, "/metrics") => {
                        let mut buffer = Vec::new();
                        let encoder = TextEncoder::new();

                        let metric_familys = prometheus::gather();
                        let result = encoder.encode(&metric_familys, &mut buffer);

                        result.map(|()| {
                            Response::new(Body::from(buffer))
                        })
                            .unwrap_or_else(|e| {
                                eprintln!("Error encoding prometheus: {}", e);
                                Response::builder()
                                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                                    .body(Body::from("PC LOAD LETTER"))
                                    .unwrap()
                            })
                    }
                    _ => {
                        Response::builder()
                            .status(StatusCode::NOT_FOUND)
                            .body(Body::empty())
                            .unwrap()
                    }
                }
            })
        };

        let addr = ([0, 0, 0, 0], 3000).into();

        let server = Server::bind(&addr)
            .serve(service)
            .map_err(|e| eprintln!("server error: {}", e));
        hyper::rt::run(server);
    }
}

